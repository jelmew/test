This is a demo project to use with openshift or s2i and docker (basically the same thing). It will launch a war in
a wildfly container and display a simple web page.

#Running with docker and S2I
If you have made the s2i image locally, you can run:
* s2i build . gradle-wildfly-centos7 docker_container_name
* run -it -p 8080:8080 docker_container-name

And it will live in a local container
#Running with openshift
##Prerequisites
If a project doesn't exist yet, create one:
oc new-project hello-world --description="an example hello-world project" --display-name="hello-world"

#
Run the application 
oc new-app gradle-wildfly-centos7~./  --strategy=source

After a language is detected, new-app searches the OpenShift Origin server for image stream tags that have a supports annotation matching the detected language, or an image stream that matches the name of the detected language. If a match is not found, new-app searches the Docker Hub registry for an image that matches the detected language based on name.

You can override the image the builder uses for a particular source repository by specifying the image (either an image stream or container specification) and the repository, with a ~ as a separator. Note that if this is done, build strategy detection and language detection are not carried out.

For example, to use the myproject/my-ruby image stream with the source in a remote repository:

$ oc new-app myproject/my-ruby~https://github.com/openshift/ruby-hello-world.git
To use the openshift/ruby-20-centos7:latest container image stream with the source in a local repository:

$ oc new-app openshift/ruby-20-centos7:latest~/home/user/code/my-ruby-app

TODO This needs somsome tweaking
Creating a java test project
oc new-project maven-java-wildfly --description="java-test-project" --display-name="maven-java-wildfly"

oc new-app --name=hello-world containername~repo_or_locatoin

oc expose service hellow-world

oc get route

oc start-build hello-world
